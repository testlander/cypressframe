/// <reference types="Cypress" />

describe('Perform Login Test Case', () => {
    it('First Test Case 01', () => {
        //Visit Execution Automation Website
        cy.visit('http://eaapp.somee.com/');
        cy.contains('Login').click();

        cy.url().should('include', 'Account/Login');

        //Enter username and password
        cy.get('#UserName').type('admin');
        cy.get('#Password').type('password');
        cy.get('input[value="Log in"]').click();
    })
})